import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import FizzBuzz from "@/views/FizzBuzz.vue";
import Vue from "vue";
import FundamentalVue from "fundamental-vue";

Vue.use(FundamentalVue);

describe("FizzBuzz.vue", () => {
  let wrapper;

  before(() => {
    wrapper = shallowMount(FizzBuzz, {});
  });

  it("isNumber()", () => {
    expect(wrapper.vm.isNumber("0")).to.equal(true);
    expect(wrapper.vm.isNumber(0)).to.equal(true);
    expect(wrapper.vm.isNumber("1")).to.equal(true);
    expect(wrapper.vm.isNumber(1)).to.equal(true);
    expect(wrapper.vm.isNumber(-1)).to.equal(true);
    expect(wrapper.vm.isNumber("x")).to.equal(false);
    expect(wrapper.vm.isNumber("123x")).to.equal(false);
    expect(wrapper.vm.isNumber("1x2")).to.equal(false);
    expect(wrapper.vm.isNumber("1x!§$!2")).to.equal(false);
    expect(wrapper.vm.isNumber('"1x!§$!2"')).to.equal(false);
  });

  it("isGreaterZero()", () => {
    expect(wrapper.vm.isGreaterZero(0)).to.equal(false);
    expect(wrapper.vm.isGreaterZero(-1)).to.equal(false);
    expect(wrapper.vm.isGreaterZero("1")).to.equal(true);
    expect(wrapper.vm.isGreaterZero("x")).to.equal(false);
    expect(wrapper.vm.isGreaterZero(1)).to.equal(true);
  });

  it("isValidWordInput()", () => {
    let rule = { number: "1", word: "" };
    expect(wrapper.vm.isValidWordInput(rule)).to.equal(false);
    rule = { number: "1", word: "x" };
    expect(wrapper.vm.isValidWordInput(rule)).to.equal(true);
  });

  it("isValidNumberInput()", () => {
    let rule = { number: "", word: "" };
    expect(wrapper.vm.isValidNumberInput(rule)).to.equal(false);
    rule = { number: "1", word: "x" };
    expect(wrapper.vm.isValidNumberInput(rule)).to.equal(true);
    rule = { number: "-1", word: "x" };
    expect(wrapper.vm.isValidNumberInput(rule)).to.equal(false);
  });

  it("areRulesValid()", () => {
    let rules = [];
    for (let i = 1; i < 6; i++) {
      rules.push({ number: `${i}`, word: `Word ${i}` });
    }
    expect(wrapper.vm.areRulesValid(rules)).to.equal(true);
    rules.push({ number: "", word: "X" });
    expect(wrapper.vm.areRulesValid(rules)).to.equal(false);
    rules[rules.length] = { number: "1", word: "" };
    expect(wrapper.vm.areRulesValid(rules)).to.equal(false);
  });

  it("calculate()", () => {
    let rules = [
      { number: "3", word: "fizz" },
      { number: "5", word: "buzz" },
      { number: "99", word: "sabine" }
    ];

    let expected = [
      "Only works with numbers greater than 0",
      "1",
      "2",
      "fizz",
      "4",
      "buzz",
      "fizz"
    ];

    expected.forEach((expectedResult, index) => {
      wrapper.vm.calculate("" + index, rules);
      expect(wrapper.vm.result, `index: ${index}`).to.equal(expectedResult);
    });

    wrapper.vm.calculate("99", rules);
    expect(wrapper.vm.result).to.equal("fizzsabine");

    wrapper.vm.calculate("15", rules);
    expect(wrapper.vm.result).to.equal("fizzbuzz");
  });
});
